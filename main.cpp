#include <Plasma/PluginLoader>
#include <Plasma/Applet>

#include <QApplication>

int main(int argc, char** argv) {

    QApplication app(argc, argv);

    const QStringList applets = {

    "org.kde.plasma.folder",
    "org.kde.plasma.folder",
    "org.kde.panel",
    "org.kde.plasma.digitalclock",
    "org.kde.plasma.showdesktop",
    "org.kde.plasma.kickoff",
    "org.kde.plasma.folder",
    "org.kde.plasma.pager",
    "org.kde.plasma.icontasks",
    "org.kde.plasma.marginsseparator",
    "org.kde.plasma.systemtray",
    "org.kde.plasma.private.systemtray",
    "org.kde.plasma.keyboardindicator",
    "org.kde.plasma.nightcolorcontrol",
    "org.kde.plasma.volume",
    "org.kde.plasma.printmanager",
    "org.kde.kdeconnect",
    "org.kde.plasma.devicenotifier",
    "org.kde.plasma.clipboard",
    "org.kde.plasma.notifications",
    "org.kde.plasma.keyboardlayout",
    "org.kde.plasma.vault",
    "org.kde.plasma.private.systemtray",
    "org.kde.plasma.keyboardindicator",
    "org.kde.plasma.nightcolorcontrol",
    "org.kde.plasma.volume",
    "org.kde.plasma.printmanager",
    "org.kde.kdeconnect",
    "org.kde.plasma.devicenotifier",
    "org.kde.plasma.clipboard",
    "org.kde.plasma.notifications",
    "org.kde.plasma.keyboardlayout",
    "org.kde.plasma.vault",
    "org.kde.plasma.private.systemtray",
    "org.kde.plasma.keyboardindicator",
    "org.kde.plasma.nightcolorcontrol",
    "org.kde.plasma.volume",
    "org.kde.plasma.printmanager",
    "org.kde.kdeconnect",
    "org.kde.plasma.devicenotifier",
    "org.kde.plasma.clipboard",
    "org.kde.plasma.notifications",
    "org.kde.plasma.keyboardlayout",
    "org.kde.plasma.vault",
    "org.kde.plasma.private.systemtray",
    "org.kde.plasma.keyboardindicator",
    "org.kde.plasma.nightcolorcontrol",
    "org.kde.plasma.volume",
    "org.kde.plasma.printmanager",
    "org.kde.kdeconnect",
    "org.kde.plasma.devicenotifier",
    "org.kde.plasma.clipboard",
    "org.kde.plasma.notifications",
    "org.kde.plasma.keyboardlayout",
    "org.kde.plasma.vault",
    "org.kde.plasma.folder",
    "org.kde.plasma.networkmanagement",
    "org.kde.plasma.bluetooth",
    "org.kde.plasma.battery",
    "org.kde.plasma.networkmanagement",
    "org.kde.plasma.bluetooth",
    "org.kde.plasma.battery",
    "org.kde.plasma.networkmanagement",
    "org.kde.plasma.bluetooth",
    "org.kde.plasma.battery",
    "org.kde.plasma.networkmanagement",
    "org.kde.plasma.bluetooth",
    "org.kde.plasma.battery"
    };

    for (const QString &applet : applets) {
        Plasma::Applet *a = Plasma::PluginLoader::self()->loadApplet(applet, 0, {});
        qDebug() << a->title();
    }
}
